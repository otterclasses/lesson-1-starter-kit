// Let's make a background for our game and give it a file location
var background = new Image();
background.src = "background.jpg";

// Now let's make our favorite kitty pusheen.
var pusheen = {};
pusheen.image = new Image();
pusheen.image.src = "pusheen.png";
pusheen.x = 100;
pusheen.y = 100;


function update(dt) {

}
	
/**
 * This function is called right after update is called.
 * It clears the screen, draws the background, then then
 * draws pusheen.
 */
function render(dt) {
    // Clears the screen with white
	ctx.save();
    ctx.clearRect(0, 0, 640, 480);
    
	// Draws the background at the origin! (comment out to see what happens part2)
	ctx.drawImage(background, 0, 0, 640, 480);
    
	// Now let's draw pusheen at their position x,y
    ctx.drawImage(pusheen.image, pusheen.x, pusheen.y, 100, 64);
	
	ctx.restore();
}

function init() {
    setInterval(function() {
        update(60/1000);
        render(60/1000);
    }, 1000/60);
}